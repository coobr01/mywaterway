Cypress - To get the "npm run coverage" command to work with the "Run all integrations" button add the following line to the cypress.json file:

  "numTestsKeptInMemory": 0